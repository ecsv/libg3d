Source: libg3d
Section: libs
Priority: optional
Maintainer: Sven Eckelmann <sven@narfation.org>
Standards-Version: 4.7.2
Rules-Requires-Root: no
Homepage: http://automagically.de/g3dviewer/
Vcs-Git: https://salsa.debian.org/ecsv/libg3d.git -b debian/unstable
Vcs-Browser: https://salsa.debian.org/ecsv/libg3d
Build-Depends:
 automake,
 debhelper-compat (= 13),
 flex,
 gtk-doc-tools,
 libgdk-pixbuf-2.0-dev,
 libglib2.0-dev,
 libgsf-1-dev | libgsf-gnome-1-dev,
 libtool,
 libxml2-dev,
 libz-dev,
 pkgconf,
Build-Conflicts:
 autoconf2.13,
 automake1.4,

Package: libg3d-dev
Section: libdevel
Architecture: any
Multi-Arch: same
Depends:
 libg3d0 (= ${binary:Version}),
 libglib2.0-dev,
 ${misc:Depends},
Recommends:
 pkgconf,
Suggests:
 libgsf-1-dev | libgsf-gnome-1-dev,
Description: LibG3D development package
 LibG3D is a framework for loading 3d model files from files or memory. It can
 also load associated data like textures.
 .
 This package contains everything which is needed to link against libg3d0.

Package: libg3d-doc
Section: doc
Architecture: all
Multi-Arch: foreign
Depends:
 ${misc:Depends},
Description: LibG3D API documentation in HTML format
 LibG3D is a framework for loading 3d model files from files or memory. It can
 also load associated data like textures.
 .
 This package contains the API documentation of the LibG3D library in HTML
 format.

Package: libg3d-plugin-gdkpixbuf
Architecture: any
Multi-Arch: same
Pre-Depends:
 ${misc:Pre-Depends},
Depends:
 libg3d0 (= ${binary:Version}),
 ${misc:Depends},
 ${shlibs:Depends},
Description: plugin for the LibG3D library
 LibG3D is a framework for loading 3d model files from files or memory. It can
 also load associated data like textures.
 .
 This package contains a plugin for the LibG3D library so that LibG3D can load
 every image that libgdk-pixbuf2 is able to work with.

Package: libg3d-plugins
Architecture: any
Multi-Arch: same
Pre-Depends:
 ${misc:Pre-Depends},
Depends:
 libg3d0 (= ${binary:Version}),
 ${misc:Depends},
 ${shlibs:Depends},
Description: plugins for the LibG3D library
 LibG3D is a framework for loading 3d model files from files or memory. It can
 also load associated data like textures.
 .
 This package contains plugins for the LibG3D library, so that files can be
 loaded.
 It can handle:
    * 3D Studio (.3ds, .prj)
    * LightWave (.lw, .lwb, .lwo)
    * Alias Wavefront (.obj)
    * Impulse TurboSilver / Imagine (.iob)
    * AutoCAD (.dxf)
    * Quake II Models (.md2)
    * Quake III Models (.md3)
    * Neutral File Format (.nff)
    * 3D Metafile (.3dmf, .3mf, .b3d)
    * Caligari TrueSpace Objects (.cob)
    * Quick3D Objects & Scenes (.q3o, q3s)
    * VRML 1.0 files (.wrl, .vrml)
    * AC3D objects (.ac, .acc)
    * LeoCAD Models (.lcd)
    * Racer car models (.ar, .dof)
    * Ultimate Stunts car models (.glb)
    * VDrift car models (.joe, .car)
    * COLLADA (.dae)
    * Keyhole Markup Language model container (.kmz)
    * ASCII Scene Exporter (.ase)
    * LDraw (.dat, .mpd)

Package: libg3d0
Architecture: any
Multi-Arch: same
Pre-Depends:
 ${misc:Pre-Depends},
Depends:
 ${misc:Depends},
 ${shlibs:Depends},
Recommends:
 libg3d-plugins,
Suggests:
 libg3d-doc,
 libg3d-plugin-gdkpixbuf,
Description: library for loading and manipulating 3D model files
 LibG3D is a framework for loading 3d model files from files or memory. It can
 also load associated data like textures.
 .
 LibG3D cannot load anything without the importers provided by libg3d-plugins
 and libg3d-plugin-gdkpixbuf. See these packages for more information about
 supported formats.
