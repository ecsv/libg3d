Format: https://www.debian.org/doc/packaging-manuals/copyright-format/1.0/
Upstream-Name: libg3d
Upstream-Contact: Markus Dahms <mad@automagically.de>
Source: http://automagically.de/git/libg3d/

Files: *
Copyright: 2005-2009, Markus Dahms <mad@automagically.de>
           2006, Romain Behar
           2006, Oliver Dippel <o.dippel@gmx.de>
           2006, Marek Lindner <marek.lindner@mailbox.org>
           2006, Simon Wunderlich <sw@simonwunderlich.de>
           2008, Peter Fritzsche <peter.fritzsche@gmx.de>
           2008, Sven Eckelmann <sven@narfation.org>
           2008, Martin Gerhardy <martin.gerhardy@gmail.com>
License: LGPL-2.1+

Files: config/*
Copyright: 1996-2001, 2003-2005, 2006
License: GPL-2+

Files: debian/*
Copyright: 2006, Timo Schneider <timo.schneider@s2004.tu-chemnitz.de>
           2008-2022, Sven Eckelmann <sven@narfation.org>
License: GPL-2+

Files: plugins/import/imp_lwo/imp_lwo.c
Copyright: 2005-2009, Markus Dahms <mad@automagically.de>
  2005, 2006, Markus Dahms
  1998, Janne Löf <jlof@mail.student.oulu.fi>
License: LGPL-2.1+

Files: plugins/import/imp_stl/*
Copyright: 2006, Oliver Dippel <o.dippel@gmx.de>
License: LGPL-2.1+

Files: src/quat.c
Copyright: 2005-2009, Markus Dahms <mad@automagically.de>
  1993, 1994, Silicon Graphics, Inc
License: LGPL-2.1+

Files: tests/test_cxx.cxx
Copyright: 2005-2008, Markus Dahms <mad@automagically.de>
License: LGPL-2.1+

License: GPL-2+
 This program is free software; you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation; version 2 dated June, 1991, or (at
 your option) any later version.
 .
 On Debian systems, the complete text of version 2 of the GNU General
 Public License can be found in '/usr/share/common-licenses/GPL-2'.

License: LGPL-2.1+
 This program is free software; you can redistribute it and/or modify it
 under the terms of the GNU Lesser General Public License as published by the
 Free Software Foundation; version 2.1 of the License, or (at
 your option) any later version.
 .
 On Debian systems, the complete text of version 2.1 of the GNU Lesser
 General Public License can be found in  `/usr/share/common-licenses/LGPL-2.1'.
