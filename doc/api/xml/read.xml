<?xml version="1.0"?>
<!DOCTYPE refentry PUBLIC "-//OASIS//DTD DocBook XML V4.1.2//EN"
               "http://www.oasis-open.org/docbook/xml/4.1.2/docbookx.dtd">
<refentry id="libg3d-read">
<refmeta>
<refentrytitle role="top_of_page" id="libg3d-read.top_of_page">read</refentrytitle>
<manvolnum>3</manvolnum>
<refmiscinfo>LIBG3D Library</refmiscinfo>
</refmeta>

<refnamediv>
<refname>read</refname>
<refpurpose>file reading abstraction (deprecated)</refpurpose>
<!--[<xref linkend="desc" endterm="desc.title"/>]-->
</refnamediv>

<refsynopsisdiv id="libg3d-read.synopsis" role="synopsis">
<title role="synopsis.title">Synopsis</title>

<synopsis>

#include &lt;g3d/read.h&gt;

<link linkend="gint32">gint32</link>              <link linkend="g3d-read-int8">g3d_read_int8</link>                       (<link linkend="FILE:CAPS">FILE</link> *f);
<link linkend="gint32">gint32</link>              <link linkend="g3d-read-int16-be">g3d_read_int16_be</link>                   (<link linkend="FILE:CAPS">FILE</link> *f);
<link linkend="gint32">gint32</link>              <link linkend="g3d-read-int16-le">g3d_read_int16_le</link>                   (<link linkend="FILE:CAPS">FILE</link> *f);
<link linkend="gint32">gint32</link>              <link linkend="g3d-read-int32-be">g3d_read_int32_be</link>                   (<link linkend="FILE:CAPS">FILE</link> *f);
<link linkend="gint32">gint32</link>              <link linkend="g3d-read-int32-le">g3d_read_int32_le</link>                   (<link linkend="FILE:CAPS">FILE</link> *f);
<link linkend="G3DFloat">G3DFloat</link>            <link linkend="g3d-read-float-be">g3d_read_float_be</link>                   (<link linkend="FILE:CAPS">FILE</link> *f);
<link linkend="G3DFloat">G3DFloat</link>            <link linkend="g3d-read-float-le">g3d_read_float_le</link>                   (<link linkend="FILE:CAPS">FILE</link> *f);
<link linkend="G3DDouble">G3DDouble</link>           <link linkend="g3d-read-double-be">g3d_read_double_be</link>                  (<link linkend="FILE:CAPS">FILE</link> *f);
<link linkend="G3DDouble">G3DDouble</link>           <link linkend="g3d-read-double-le">g3d_read_double_le</link>                  (<link linkend="FILE:CAPS">FILE</link> *f);
<link linkend="gint32">gint32</link>              <link linkend="g3d-read-cstr">g3d_read_cstr</link>                       (<link linkend="FILE:CAPS">FILE</link> *f,
                                                         <link linkend="gchar">gchar</link> *buffer,
                                                         <link linkend="gint32">gint32</link> max_len);
</synopsis>
</refsynopsisdiv>









<refsect1 id="libg3d-read.description" role="desc">
<title role="desc.title">Description</title>
<para>
The g3d_read_* family of functions is deprecated in favour of the <link linkend="G3DStream"><type>G3DStream</type></link>
interface.</para>
<para>

</para>
</refsect1>

<refsect1 id="libg3d-read.details" role="details">
<title role="details.title">Details</title>
<refsect2 id="g3d-read-int8" role="function">
<title>g3d_read_int8 ()</title>
<indexterm zone="g3d-read-int8"><primary>g3d_read_int8</primary></indexterm><programlisting><link linkend="gint32">gint32</link>              g3d_read_int8                       (<link linkend="FILE:CAPS">FILE</link> *f);</programlisting>
<para>
Read a 1 byte signed integer from file.</para>
<para>

</para><variablelist role="params">
<varlistentry><term><parameter>f</parameter>&nbsp;:</term>
<listitem><simpara> the file to read from
</simpara></listitem></varlistentry>
<varlistentry><term><emphasis>Returns</emphasis>&nbsp;:</term><listitem><simpara> The read value, 0 in case of error
</simpara></listitem></varlistentry>
</variablelist></refsect2>
<refsect2 id="g3d-read-int16-be" role="function">
<title>g3d_read_int16_be ()</title>
<indexterm zone="g3d-read-int16-be"><primary>g3d_read_int16_be</primary></indexterm><programlisting><link linkend="gint32">gint32</link>              g3d_read_int16_be                   (<link linkend="FILE:CAPS">FILE</link> *f);</programlisting>
<para>
Read a 2 byte big-endian signed integer from file.</para>
<para>

</para><variablelist role="params">
<varlistentry><term><parameter>f</parameter>&nbsp;:</term>
<listitem><simpara> the file to read from
</simpara></listitem></varlistentry>
<varlistentry><term><emphasis>Returns</emphasis>&nbsp;:</term><listitem><simpara> The read value, 0 in case of error
</simpara></listitem></varlistentry>
</variablelist></refsect2>
<refsect2 id="g3d-read-int16-le" role="function">
<title>g3d_read_int16_le ()</title>
<indexterm zone="g3d-read-int16-le"><primary>g3d_read_int16_le</primary></indexterm><programlisting><link linkend="gint32">gint32</link>              g3d_read_int16_le                   (<link linkend="FILE:CAPS">FILE</link> *f);</programlisting>
<para>
Read a 2 byte little-endian signed integer from file.</para>
<para>

</para><variablelist role="params">
<varlistentry><term><parameter>f</parameter>&nbsp;:</term>
<listitem><simpara> the file to read from
</simpara></listitem></varlistentry>
<varlistentry><term><emphasis>Returns</emphasis>&nbsp;:</term><listitem><simpara> The read value, 0 in case of error
</simpara></listitem></varlistentry>
</variablelist></refsect2>
<refsect2 id="g3d-read-int32-be" role="function">
<title>g3d_read_int32_be ()</title>
<indexterm zone="g3d-read-int32-be"><primary>g3d_read_int32_be</primary></indexterm><programlisting><link linkend="gint32">gint32</link>              g3d_read_int32_be                   (<link linkend="FILE:CAPS">FILE</link> *f);</programlisting>
<para>
Read a 4 byte big-endian signed integer from file.</para>
<para>

</para><variablelist role="params">
<varlistentry><term><parameter>f</parameter>&nbsp;:</term>
<listitem><simpara> the file to read from
</simpara></listitem></varlistentry>
<varlistentry><term><emphasis>Returns</emphasis>&nbsp;:</term><listitem><simpara> The read value, 0 in case of error
</simpara></listitem></varlistentry>
</variablelist></refsect2>
<refsect2 id="g3d-read-int32-le" role="function">
<title>g3d_read_int32_le ()</title>
<indexterm zone="g3d-read-int32-le"><primary>g3d_read_int32_le</primary></indexterm><programlisting><link linkend="gint32">gint32</link>              g3d_read_int32_le                   (<link linkend="FILE:CAPS">FILE</link> *f);</programlisting>
<para>
Read a 4 byte little-endian signed integer from file.</para>
<para>

</para><variablelist role="params">
<varlistentry><term><parameter>f</parameter>&nbsp;:</term>
<listitem><simpara> the file to read from
</simpara></listitem></varlistentry>
<varlistentry><term><emphasis>Returns</emphasis>&nbsp;:</term><listitem><simpara> The read value, 0 in case of error
</simpara></listitem></varlistentry>
</variablelist></refsect2>
<refsect2 id="g3d-read-float-be" role="function">
<title>g3d_read_float_be ()</title>
<indexterm zone="g3d-read-float-be"><primary>g3d_read_float_be</primary></indexterm><programlisting><link linkend="G3DFloat">G3DFloat</link>            g3d_read_float_be                   (<link linkend="FILE:CAPS">FILE</link> *f);</programlisting>
<para>
Read a 4 byte big-endian floating point number from file.</para>
<para>

</para><variablelist role="params">
<varlistentry><term><parameter>f</parameter>&nbsp;:</term>
<listitem><simpara> the file to read from
</simpara></listitem></varlistentry>
<varlistentry><term><emphasis>Returns</emphasis>&nbsp;:</term><listitem><simpara> The read value, 0 in case of error
</simpara></listitem></varlistentry>
</variablelist></refsect2>
<refsect2 id="g3d-read-float-le" role="function">
<title>g3d_read_float_le ()</title>
<indexterm zone="g3d-read-float-le"><primary>g3d_read_float_le</primary></indexterm><programlisting><link linkend="G3DFloat">G3DFloat</link>            g3d_read_float_le                   (<link linkend="FILE:CAPS">FILE</link> *f);</programlisting>
<para>
Read a 4 byte little-endian floating point number from file.</para>
<para>

</para><variablelist role="params">
<varlistentry><term><parameter>f</parameter>&nbsp;:</term>
<listitem><simpara> the file to read from
</simpara></listitem></varlistentry>
<varlistentry><term><emphasis>Returns</emphasis>&nbsp;:</term><listitem><simpara> The read value, 0 in case of error
</simpara></listitem></varlistentry>
</variablelist></refsect2>
<refsect2 id="g3d-read-double-be" role="function">
<title>g3d_read_double_be ()</title>
<indexterm zone="g3d-read-double-be"><primary>g3d_read_double_be</primary></indexterm><programlisting><link linkend="G3DDouble">G3DDouble</link>           g3d_read_double_be                  (<link linkend="FILE:CAPS">FILE</link> *f);</programlisting>
<para>
Read a 8 byte big-endian double-precision floating point number from file.</para>
<para>

</para><variablelist role="params">
<varlistentry><term><parameter>f</parameter>&nbsp;:</term>
<listitem><simpara> the file to read from
</simpara></listitem></varlistentry>
<varlistentry><term><emphasis>Returns</emphasis>&nbsp;:</term><listitem><simpara> The read value, 0 in case of error
</simpara></listitem></varlistentry>
</variablelist></refsect2>
<refsect2 id="g3d-read-double-le" role="function">
<title>g3d_read_double_le ()</title>
<indexterm zone="g3d-read-double-le"><primary>g3d_read_double_le</primary></indexterm><programlisting><link linkend="G3DDouble">G3DDouble</link>           g3d_read_double_le                  (<link linkend="FILE:CAPS">FILE</link> *f);</programlisting>
<para>
Read a 8 byte little-endian double-precision floating point number from
file.</para>
<para>

</para><variablelist role="params">
<varlistentry><term><parameter>f</parameter>&nbsp;:</term>
<listitem><simpara> the file to read from
</simpara></listitem></varlistentry>
<varlistentry><term><emphasis>Returns</emphasis>&nbsp;:</term><listitem><simpara> The read value, 0 in case of error
</simpara></listitem></varlistentry>
</variablelist></refsect2>
<refsect2 id="g3d-read-cstr" role="function">
<title>g3d_read_cstr ()</title>
<indexterm zone="g3d-read-cstr"><primary>g3d_read_cstr</primary></indexterm><programlisting><link linkend="gint32">gint32</link>              g3d_read_cstr                       (<link linkend="FILE:CAPS">FILE</link> *f,
                                                         <link linkend="gchar">gchar</link> *buffer,
                                                         <link linkend="gint32">gint32</link> max_len);</programlisting>
<para>
Read a zero-terminated string from file.</para>
<para>

</para><variablelist role="params">
<varlistentry><term><parameter>f</parameter>&nbsp;:</term>
<listitem><simpara> the file to read from
</simpara></listitem></varlistentry>
<varlistentry><term><parameter>buffer</parameter>&nbsp;:</term>
<listitem><simpara> the buffer to store line in
</simpara></listitem></varlistentry>
<varlistentry><term><parameter>max_len</parameter>&nbsp;:</term>
<listitem><simpara> maximum length of string including termination character
</simpara></listitem></varlistentry>
<varlistentry><term><emphasis>Returns</emphasis>&nbsp;:</term><listitem><simpara> number of bytes read from file.
</simpara></listitem></varlistentry>
</variablelist></refsect2>

</refsect1>



<refsect1 id="libg3d-read.see-also">
<title>See Also</title>
g3d_stream_open_file
</refsect1>

</refentry>
